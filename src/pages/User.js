import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'
import { Link } from 'react-router-dom'
import Icon from '@material-ui/core/Icon'
import ArticleBox from '../components/ArticleBoxWrapper/ArticleBoxWrapper'
import Title from '../components/Title/Title'
import Text from '../components/Text/Text'

import { title } from 'utils'

import { userService } from 'services'

class UserPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            list: [],
            id: this.props.match.params.id,
            user: {},
            value: '',
        }
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(id) {
        this.setState({
            value: id.target.value,
        })
        const getUserInfo = async (id) => {
            const data = await userService
                .getUserData(id)
                .then((e) => e)
                .catch((e) => {
                    console.log(e)
                })
            console.log(data)
            this.setState({
                user: data,
            })
            console.log(this.state.user)
        }
        getUserInfo(id.target.value)
    }

    componentDidMount() {
        const getList = async () => {
            const data = await userService
                .list()
                .then((e) => e)
                .catch((e) => {
                    console.log(e)
                })
            console.log(data)
            this.setState({
                list: data,
            })
        }
        getList()
    }

    render() {
        return (
            <Fragment>
                <Helmet>{title('Page secondaire')}</Helmet>

                <div className="wrap page-wrap ">
                    <div className="arrowTop">
                        <Link to="/" className="nav-arrow">
                            <Icon
                                style={{
                                    transform: 'rotate(180deg)',
                                    fontSize: 100,
                                }}
                            >
                                arrow_right_alt
                            </Icon>
                        </Link>
                    </div>

                    <div className="users-select">
                        <h1>
                            {/* Liste dynamique à partir de l'API */}
                            <select
                                value={this.state.value}
                                onChange={this.handleChange}
                                className="dropdown"
                            >
                                {this.state.list.map((item, i) => (
                                    <option value={item.id} key={i}>
                                        Utilisateur # {item.id}
                                    </option>
                                ))}
                            </select>
                        </h1>
                    </div>
                    <div className="infos-block">
                        {/* Infos dynamiques sur l'utilisateur sélectionné */}
                        <h3>
                            occupation:{' '}
                            <span
                                style={{
                                    fontSize: 30,
                                    color: 'white ',
                                }}
                            >
                                {this.state.user.occupation}
                            </span>
                        </h3>
                        <h3>
                            Date de naissance:{' '}
                            <span
                                style={{
                                    fontSize: 30,
                                    color: 'white ',
                                }}
                            >
                                {this.state.user.birthdate}
                            </span>{' '}
                        </h3>
                    </div>

                    <div className="article-list">
                        {this.state.user && this.state.user.articles
                            ? this.state.user.articles.map((item) => {
                                  return (
                                      <div className="grid-container">
                                          <div className="grid-item">
                                              <ArticleBox key={item.name}>
                                                  <Title>{item.name}</Title>
                                                  <Text>
                                                      {item.content.substring(
                                                          0,
                                                          300
                                                      )}
                                                  </Text>
                                              </ArticleBox>
                                          </div>
                                      </div>
                                  )
                              })
                            : undefined}
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default UserPage
