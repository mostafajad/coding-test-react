import React, { Fragment } from "react";
import Helmet from "react-helmet";
import { Link } from "react-router-dom";
import Icon from "@material-ui/core/Icon";
//My new Component
import BallLeft from "../components/BallLeft/ballLeft";
import BallRight from "../components/BallRight/ballRight";

import { title } from "utils";

//Updated to function componets
const HomePage = () => {
  return (
    <Fragment>
      <Helmet>{title("Page d'accueil")}</Helmet>
      {/* *  Created new componet I would have done it better using styled componets with props*/}
      <BallLeft />
      <BallRight />

      <div className="wrapper page-wrap">
        <h1 className="title">04h11</h1>
        <p className="subTitle">Spécialiste de vos données.</p>
        <div className="arrowBottom">
          <Link to="/users" className="nav-arrow">
            {/* Matrial UI Style */}
            <Icon style={{ fontSize: 100 }} className="icon">
              arrow_right_alt
            </Icon>
          </Link>
        </div>
      </div>
    </Fragment>
  );
};

export default HomePage;
