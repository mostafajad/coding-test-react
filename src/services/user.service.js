import { API } from "constants/api";
import axios from "axios";

export const userService = {
  list,
  getUserData,
};

function list() {
  const getInfo = { method: "GET" };
  return new Promise((resolve, reject) => {
    fetch(`${API}/users/`, getInfo)
      .then((result) => {
        return result.json();
      })
      .then((result) => {
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function getUserData(id) {
  const getId = { method: "GET" };
  return new Promise((resolve, reject) => {
    fetch(`${API}/users/${id}`, getId)
      .then((result) => {
        return result.json();
      })
      .then((result) => {
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
// get user id
//get user info by id
//store the  in a state
//map through the sel
// display the data for each user
