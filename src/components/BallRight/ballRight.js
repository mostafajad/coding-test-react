import React from "react";
import "./_ball.scss";
const BallRight = () => {
  return (
    <div className="background">
      <div className="right">
        <div className="ballRight"></div>
      </div>
    </div>
  );
};

export default BallRight;
