import React from "react";
import "./_ball.scss";
const BallLeft = () => {
  return (
    <div className="background">
      <div className="left">
        <div className="ballLeft"></div>
      </div>
    </div>
  );
};

export default BallLeft;
