import React from "react";
import "./_title.scss";

const Title = ({ children }) => {
  return (
    <div className="titleText">
      <h1>{children}</h1>
    </div>
  );
};

export default Title;
