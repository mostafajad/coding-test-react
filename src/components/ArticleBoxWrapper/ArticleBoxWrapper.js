import React from "react";
import "./_articleBoxWrapper.scss";

const ArticleBox = ({ children }) => {
  return <div className="backgroundBox">{children}</div>;
};

export default ArticleBox;
